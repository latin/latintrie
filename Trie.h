//
// Created by Andrew Toshiaki Nakayama Kurauchi on 4/5/17.
//

#ifndef TRIE_H
#define TRIE_H

#include <map>
#include <vector>
#include <string>

struct WordOccurrence
{
    /**
     * Constructor.
     *
     * @param word the word
     * @param occurrences number of occurrences of the word in the corpus
     */
    WordOccurrence(std::string word, long occurrences);
    std::string word;
    long occurrences;
};

class Trie
{
public:
    Trie();
    /**
     *  Constructor.
     *  If a csv file is provided, the constructor assumes that the delimiter is a tab character (\t), the occurrence
     *  information is in the first column and the word on the second.
     *
     * @param filename name of the file containing words to be added (can be .txt or .csv)
     */
    Trie(std::string filename);
    ~Trie();
    /**
     * Loads the words in the file.
     *
     * @param wordListFile file with one word per line
     */
    void load(std::string wordListFile);
    /**
     * Loads the words in the csv file.
     *
     * @param wordListFile csv file with information about one word per line
     * @param delimiter csv delimiter (default = tab character - \t)
     * @param wordIdx column index of the word (default = 1)
     * @param freqIdx column index of the occurrence information (default = 0)
     * @param occurrencesOnly if true, no new word is added, only the occurrences (default = false)
     */
    void loadCsv(std::string wordListFile, char delimiter = '\t', int wordIdx = 1, int freqIdx = 0, bool occurrencesOnly = false);
    /**
     * Add word to the trie.
     *
     * @param word word to be added
     * @param occurrences occurrences of that word in the corpus (default = 1)
     */
    void addWord(const std::string &word, long occurrences = 1);
    /**
     * Removes one occurrence of the word.
     * @param word word to have occurrence removed (only one occurrence is removed)
     */
    void removeOccurrence(const std::string &word);
    /**
     * Checks if word is in the trie.
     *
     * @param word target word
     * @return true if word is in the trie, false otherwise
     */
    bool contains(const std::string &word);
    /**
     * Find the number of occurrences of the word.
     *
     * @param word target word
     * @return number of occurrences of the word in the corpus.
     */
    long getOccurrencesFor(const std::string &word);
    /**
     * Lists all words that can be written with exactly one letter from each bucket of letters.
     *
     * @param buckets vector of buckets (each bucket is represented by a string with all the letters in it)
     * @return vector of WordOccurrence objects containing the words that can be written with the buckets
     */
    std::vector<WordOccurrence> findWordsIn(std::vector<std::string> buckets);
private:
    class Node
    {
    public:
        Node();
        Node(char value);
        ~Node();
        char getValue();
        void setWord(bool word);
        bool isWord();
        long getOccurrences();
        Node *getChildAt(char pos);
        void addChild(Node *child);
        void addOccurrences(long occurrences);

    private:
        char value;
        bool word;
        long occurrences;
        std::map<char, Node*> children;

        friend class Trie;
    };

    Node *root;

    Node *findNodeFor(const std::string &word, bool createIfNotFound = false);
    std::vector<WordOccurrence> findWordsIn(std::vector<std::string> buckets, std::string curWord, Node *curNode);
};

#endif // TRIE_H
