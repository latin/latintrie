#include "Trie.h"//
// Created by Andrew Toshiaki Nakayama Kurauchi on 4/5/17.
//

#include <fstream>

#include "Trie.h"

// Auxiliary functions

static inline std::string trim(std::string &s)
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
    s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
    return s;
}

// WordOccurrence

WordOccurrence::WordOccurrence(std::string word, long occurrences) : word(word), occurrences(occurrences)
{
}

// Trie

Trie::Trie() : root(new Node)
{
}

Trie::Trie(std::string filename) : Trie()
{
    unsigned long n = filename.length();
    std::string ext = filename.substr(n - 4, n - 1);
    if (ext.compare(".txt") == 0) load(filename);
    else if (ext.compare(".csv") == 0) loadCsv(filename);
    else throw std::runtime_error("Unknown dictionary file extension");
}

Trie::~Trie()
{
    if (root != nullptr) delete root;
}

void Trie::load(std::string wordListFile)
{
    std::string word;
    std::ifstream wordList(wordListFile);
    if (wordList.is_open())
    {
        while (std::getline(wordList, word))
        {
            addWord(word);
        }
        wordList.close();
    }
}

void Trie::loadCsv(std::string wordListFile, char delimiter, int wordIdx, int freqIdx, bool occurrencesOnly)
{
    std::string row;
    std::ifstream wordList(wordListFile);
    size_t pos;

    if (wordList.is_open())
    {
        while (std::getline(wordList, row))
        {
            std::vector<std::string> wordInfo;
            while ((pos = row.find_first_of(delimiter)) != std::string::npos)
            {
                wordInfo.push_back(row.substr(0, pos));
                row.erase(0, pos + 1);
            }
            if (row.length() > 0) wordInfo.push_back(row);
            std::string word = trim(wordInfo[wordIdx]);
            if (!occurrencesOnly || contains(word))
            {
                addWord(word, std::stoi(trim(wordInfo[freqIdx])));
            }
        }
        wordList.close();
    }
}

void Trie::addWord(const std::string &word, long occurrences)
{
    Node *node = findNodeFor(word, true);
    if (node)
    {
        node->addOccurrences(occurrences);
        node->setWord(true);
    }
}

void Trie::removeOccurrence(const std::string &word)
{
    Node *node = findNodeFor(word);
    if (node) node->addOccurrences(-1);
}

bool Trie::contains(const std::string &word)
{
    Node *node = findNodeFor(word);
    return node && node->isWord();
}

long Trie::getOccurrencesFor(const std::string &word)
{
    Node *node = findNodeFor(word);
    if (node) return node->getOccurrences();
    return 0;
}

std::vector<WordOccurrence> Trie::findWordsIn(std::vector<std::string> buckets)
{
    return findWordsIn(buckets, "", root);
}

Trie::Node* Trie::findNodeFor(const std::string &word, bool createIfNotFound)
{
    Node *current = root;
    if (word.length() == 0) return nullptr;

    for (unsigned i = 0; i < word.length(); i++)
    {
        char c = (char) tolower(word[i]);
        Node *child = current->getChildAt(c);
        if (child == nullptr)
        {
            if (createIfNotFound)
            {
                child = new Node(c);
                current->addChild(child);
            }
            else
            {
                return nullptr;
            }
        }
        current = child;
    }
    return current;
}

std::vector<WordOccurrence> Trie::findWordsIn(std::vector<std::string> buckets, std::string curWord, Node *curNode)
{
    std::vector<WordOccurrence> wordOccurrences;
    if (buckets.size() == 0) return wordOccurrences;

    bool isLast = false;
    if (buckets.size() == 1) isLast = true;

    std::string firstBucket = *buckets.begin();
    std::vector<std::string> tailBuckets = std::vector<std::string>(buckets.begin() + 1, buckets.end());
    for (int i = 0; i < firstBucket.length(); i++)
    {
        char letter = firstBucket[i];
        Node *child = curNode->getChildAt(letter);
        if (child != nullptr)
        {
            if (isLast)
            {
                if (child->isWord())
                    wordOccurrences.push_back(WordOccurrence(curWord + letter, child->getOccurrences()));
            }
            else
            {
                std::vector<WordOccurrence> newOccurrences = findWordsIn(tailBuckets, curWord + letter, child);
                wordOccurrences.insert(wordOccurrences.end(), newOccurrences.begin(), newOccurrences.end());
            }
        }
    }

    return wordOccurrences;
}

// Trie::Node

Trie::Node::Node() : Node(' ')
{}

Trie::Node::Node(char value) : value(value), word(false), occurrences(0)
{}

Trie::Node::~Node()
{
    for (auto it = children.cbegin(); it != children.cend();)
    {
        Node *node = it->second;
        children.erase(it++);
        delete node;
    }
}

char Trie::Node::getValue()
{
    return value;
}

void Trie::Node::setWord(bool word)
{
    this->word = word;
}

bool Trie::Node::isWord()
{
    return word;
}

long Trie::Node::getOccurrences()
{
    return occurrences;
}

Trie::Node *Trie::Node::getChildAt(char pos)
{
    auto it = children.find(pos);
    if (it == children.end()) return nullptr;
    return it->second;
}

void Trie::Node::addChild(Trie::Node *child)
{
    children[child->getValue()] = child;
}

void Trie::Node::addOccurrences(long occurrences)
{
    this->occurrences += occurrences;
    if (this->occurrences < 1) this->occurrences = 1;
}
