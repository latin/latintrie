//
// Created by Andrew Toshiaki Nakayama Kurauchi on 4/5/17.
//

#include <iostream>
#include <vector>

#include "Trie.h"

void testContains(Trie &trie, std::string word)
{
    if (trie.contains(word))
        std::cout << "Trie contains word: " << word << " (" << trie.getOccurrencesFor(word) << " occurrences)" << std::endl;
    else
        std::cout << "Trie does not contain word: " << word << std::endl;
}

void testBuckets(Trie &trie, std::vector<std::string> buckets)
{
    if (buckets.size())
    {
        std::cout << "WORDS THAT CAN BE FORMED WITH THE BUCKETS: " << *buckets.begin();
        for (auto it = buckets.begin() + 1; it != buckets.end(); it++) std::cout << ", " << *it;
    }
    else std::cout << "WORDS THAT CAN BE FORMED WITH NO BUCKETS";
    std::cout << std::endl;
    std::vector<WordOccurrence> wordOccurrences = trie.findWordsIn(buckets);
    for (auto it = wordOccurrences.begin(); it != wordOccurrences.end(); it++)
    {
        std::cout << it->word << " (" << it->occurrences << " occurrences)" << std::endl;
    }
    if (wordOccurrences.size() == 0) std::cout << "No words can be formed with the provided buckets" << std::endl;
}

int main()
{
    std::cout << "TESTING TRIE WITHOUT OCCURRENCE DATA" << std::endl;
    Trie simpleTrie("words.txt");
    testContains(simpleTrie, "test");
    testContains(simpleTrie, "the");
    testContains(simpleTrie, "dictionary");
    testContains(simpleTrie, "thisworddoesnotexist");

    std::cout << std::endl;

    std::cout << "TESTING TRIE WITH OCCURRENCE DATA" << std::endl;
    Trie trieWithOccurrences("words.csv");
    testContains(trieWithOccurrences, "test");
    testContains(trieWithOccurrences, "the");
    testContains(trieWithOccurrences, "dictionary");
    testContains(trieWithOccurrences, "thisworddoesnotexist");

    std::cout << std::endl;

    std::vector<std::string> buckets {"phjk", "poil", "iuya", "asdc", "wer"};
    testBuckets(trieWithOccurrences, buckets);

    std::cout << std::endl;

    std::vector<std::string> emptyBuckets {"phjk", "poil", "", "asdc", "wer"};
    testBuckets(trieWithOccurrences, emptyBuckets);

    std::cout << std::endl;

    std::vector<std::string> noBuckets;
    testBuckets(trieWithOccurrences, noBuckets);

    return 0;
}
