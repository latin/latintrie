# LaTIn Trie

This is a simple example of a trie structure (prefix tree). We use this structure for word prediction and retrieval.

## Build

To build the project create a directory `build` and run cmake:

    mkdir build
    cd build
    cmake .
    make

## Demo

There is an example code on `demo/demo.cpp`. When you build the project the executable will be in the `build/demo` folder. Just run it as:

    demo/TrieDemo

It should output the following:

    TESTING TRIE WITHOUT OCCURRENCE DATA
    Trie does not contain word: test
    Trie does not contain word: the
    Trie does not contain word: dictionary
    Trie does not contain word: thisworddoesnotexist

    TESTING TRIE WITH OCCURRENCE DATA
    Trie does not contain word: test
    Trie does not contain word: the
    Trie does not contain word: dictionary
    Trie does not contain word: thisworddoesnotexist

    WORDS THAT CAN BE FORMED WITH THE BUCKETS: phjk, poil, iuya, asdc, wer
    No words can be formed with the provided buckets

    WORDS THAT CAN BE FORMED WITH THE BUCKETS: phjk, poil, , asdc, wer
    No words can be formed with the provided buckets

    WORDS THAT CAN BE FORMED WITH NO BUCKETS
    No words can be formed with the provided buckets
